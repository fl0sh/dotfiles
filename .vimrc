set nocompatible

syntax enable
set relativenumber
color wombat256mod
set numberwidth=3
set showmatch

set autoindent
set smartindent
set cindent

" cinoptions (awesome) :help cinoptions-values
set cinoptions=l1,N-s,t0,(0

set tabstop=4
set shiftwidth=4

" new windows on right side
set splitright

inoremap { <esc>:call CurlyBracket()<CR>a

" must return cursor to the line with '{'
function CurlyBracket()
	let line = getline(".")

	if  line[col(".")-1] == ")"
		call CurlyBracket_Function()

	elseif matchstr(line, "case ") == "case "
		call CurlyBracket_Case()

	else
		:execute "normal! a{"

	endif

endfunction

function CurlyBracket_Function()
	:execute "normal! o{\<CR>}\<esc>k"
endfunction

function CurlyBracket_Case()
	:execute "normal! a {\<CR>} break;\<esc>k\<S-a>"
endfunction
